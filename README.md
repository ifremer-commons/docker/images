# Registre publique d'images Docker IFREMER

Ce projet a pour but de stocker les images Docker nécessaires à la construction des différents pipelines GitLab CI pour les projets Ifremer.

Stocker les images sur un registre Ifremer et utiliser ces images est important car une limite de 100 pulls est définie depuis le Docker Hub. Si la limite est atteinte depuis une machine, l'erreur suivante apparait dans lors de l'execution d'un pipeline : `You have reached your pull rate limit. You may increase the limit by authenticating and upgrading: https://www.docker.com/increase-rate-limits.`

Il est encore plus important d'utiliser ces images lorsque l'on fait du Docker in Docker (dind) car les images ne sont pas stockées dans le cache dans ce cas précis.

Les images stockées sur cette registry sont de simples copies des images présentes sur le Docker Hub. Les images sont mises à jour automatiquement, de manière hebdomadaire, via une cron.

## Mise à jour des images

Les images sont mises à jour via une cron de manière hebdomadaire (le dimanche).

## Ajout d'une nouvelle image

Le projet étant accessible librement sur internet, il impossible d'éditer directement la branche stable (branche `main`). Créer une branche et adresser une merge request à @lb06435 (ou un [membre](https://gitlab.ifremer.fr/ifremer-commons/docker/internal-images/-/project_members) ayant à minima le role `maintainer`) pour toute modification.

### Accessible sur Dockerhub

Editer le fichier `.gitlab-ci.yml` afin d'y ajouter l'image souhaitée en suivant en utilisant le template `.dockerhub`.

Exemple pour une image avec un seul tag :

```YAML
# Youtrack images from dockerhub
dockerhub:jetbrains/youtrack:
  extends: .dockerhub
  variables:
    IMAGE_BASE: jetbrains/youtrack
    IMAGE_TAG: 2023.3.24329
```

Exemple pour une image avec plusieurs tags :

```YAML
# Ubuntu images from dockerhub
dockerhub:ubuntu:
  extends: .dockerhub
  parallel:
    matrix:
      - IMAGE_BASE: [ubuntu]
        IMAGE_TAG: ["22.04", "24.04"]
```

### Accessible une autre registry

Exemple pour une image stockée sur une registry Gitlab :

```YAML
# release-cli images from Gitlab
gitlab:release-cli:
  extends: .from:custom
  variables:
    IMAGE_REGISTRY: registry.gitlab.com
    IMAGE_PATH: gitlab-org
    IMAGE_BASE: release-cli
  parallel:
    matrix:
      - IMAGE_TAG: [v0.16.0, latest]
```

## Utiliser cette image

### En ligne de commande

```bash
docker pull gitlab-registry.ifremer.fr/ifremer-commons/docker/images/python:3.10-slim
```

### Dans un pipeline GitLab CI

```yaml
image: gitlab-registry.ifremer.fr/ifremer-commons/docker/images/python:3.10-slim
```

## Image Apache httpd

Pour lancer l'image Apache httpd sans être `root` dans le conteneur :

```bash
# 33:33 correspond à l'utilisateur www-data:www-data dans le conteneur
docker run --rm -ti -u 33:33 \
  -p 127.0.0.1:8200:80 \
  -v /path/to/your/document/root/on/the/host:/usr/local/apache2/htdocs:ro \
  gitlab-registry.ifremer.fr/ifremer-commons/docker/images/httpd:2.4 \
  httpd-foreground -C 'PidFile /tmp/httpd.pid'
```

Ensuite, pour accéder à la page d'accueil :

```bash
curl http://localhost:8200
```
